//
// Created by S. Jansen on 24-11-2018.
//

#include "MovingHead.h"

MovingHead::MovingHead(Movement *movement, NeoPixel *neoPixel) {
  this->movement = movement;
  this->lamp = neoPixel;
}

void MovingHead::begin() {
  lamp->begin();
  movement->begin();

  update();
}

void MovingHead::setPositionX(uint8_t value){
  // Check if change is needed
  if (value == position_X)
    return;

  position_X = value;
  updatePositionX = true;
}

void MovingHead::setPositionY(uint8_t value){
  // Check if change is needed
  if (value == position_Y)
    return;

  position_Y = value;
  updatePositionY = true;
}

void MovingHead::setColor(uint32_t value){
  // Check if change is needed
  if (value == color)
    return;

  color = value;
  updateColor = true;
}

void MovingHead::setColorR(uint8_t value) {
    uint32_t newColor = color & ~((uint32_t) 0xFF << 16);
    setColor(newColor | (uint32_t) value << 16);
}

void MovingHead::setColorG(uint8_t value) {
    uint32_t newColor = color & ~((uint32_t) 0xFF << 8);
    setColor(newColor | (uint32_t) value << 8);
}

void MovingHead::setColorB(uint8_t value) {
    uint32_t newColor = color & ~((uint32_t) 0xFF);
    setColor(newColor | (uint32_t) value);
}

void MovingHead::setBrightness(uint8_t value){
  // Check if change is needed
  if (value == brightness)
    return;

  brightness = value;
  updateBrightness = true;
}

void MovingHead::setEffect(uint8_t value){
  // Check if change is needed
  if (value == effect)
    return;

  effect = (NeoPixel::Effect) value;
  updateEffect = true;
}

void MovingHead::setEffectSpeed(uint16_t speed){
  // Check if change is needed
  if (speed == effectSpeed)
    return;

  effectSpeed = speed;
  updateEffectpeed = true;
}

void MovingHead::update(){
  if (updatePositionX)
    movement->toX(position_X);
  if (updatePositionY)
    movement->toY(position_Y);

  if (updateColor)
    lamp->setColor(color);
  if (updateBrightness)
    lamp->setBrightness(brightness);
  if (updateEffect)
    lamp->setEffect(effect);
  if (updateEffectpeed)
    lamp->setEffectSpeed(effectSpeed);

  updatePositionX = false;
  updatePositionY = false;
  updateColor = false;
  updateBrightness = false;
  updateEffect = false;
  updateEffectpeed = false;
}


uint8_t MovingHead::getPositionX(){
  return movement->getServoPosX();
}

uint8_t MovingHead::getPositionY(){
  return movement->getServoPosY();
}

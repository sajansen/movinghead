//
// Created by S. Jansen on 24-11-2018.
//

#ifndef MOVINGHEAD_MOVINGHEAD_H
#define MOVINGHEAD_MOVINGHEAD_H

#include <Arduino.h>
#include "NeoPixel.h"
#include "Movement.h"

class MovingHead {
  public:
    MovingHead(){}
    MovingHead(Movement *movement, NeoPixel *neoPixel);

    void begin();

    void setPositionX(uint8_t value);
    void setPositionY(uint8_t value);
    void setColor(uint32_t value);
    void setColorR(uint8_t value);
    void setColorG(uint8_t value);
    void setColorB(uint8_t value);
    void setBrightness(uint8_t value);
    void setEffect(uint8_t value);
    void setEffectSpeed(uint16_t duration);
    void update();

    uint8_t getPositionX();
    uint8_t getPositionY();

    Movement *getMovement(){ return movement;}

  private:
    Movement *movement;
    NeoPixel *lamp;

    uint8_t position_X = 0;
    uint8_t position_Y = 0;
    uint32_t color = 0;
    uint8_t brightness = 10;
    NeoPixel::Effect effect = NeoPixel::OFF;
    uint16_t effectSpeed = 500;

    boolean updatePositionX = true;
    boolean updatePositionY = true;
    boolean updateColor = true;
    boolean updateBrightness = true;
    boolean updateEffect = true;
    boolean updateEffectpeed = true;
};


#endif //MOVINGHEAD_MOVINGHEAD_H

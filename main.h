//
// Created by S. Jansen on 24-11-2018.
//

#ifndef MOVINGHEAD_PARENT_H
#define MOVINGHEAD_PARENT_H

#include <Arduino.h>
#include "lib/movingheadcommunicationprotocol/MovingHeadCommunicationProtocol.h"
#include "NeoPixel.h"
#include "Movement.h"
#include "MovingHead.h"
#include "Demo.h"

#define NEOPIXEL_PIN 6
#define SERVO_PIN_X 9
#define SERVO_PIN_Y 10
#define EXTERNAL_DEMO_SWITCH_PIN 2      // Init demo or not on the hand of an external switch (useful in case no Serial controller is available). Only checks at startup.

// Interfaces
//    Serial:
#define USE_SERIAL false
#define BAUD_RATE 11920
//    I2C/Wire:
#define USE_I2C true
#define I2C_ADDRESS 8
#define I2C_DATA_BUFFER_SIZE 255

#endif //MOVINGHEAD_PARENT_H

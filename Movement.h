//
// Created by S. Jansen on 24-11-2018.
//

#ifndef MOVINGHEAD_MOVEMENT_H
#define MOVINGHEAD_MOVEMENT_H

#include <Arduino.h>
//#include <Servo.h>
#include "HServo.h"

class Movement {
  public:
    Movement(uint8_t servoPinX, uint8_t servoPinY);
    void begin();
    // Set new position
    void toX(uint8_t pos);
    void toY(uint8_t pos);
    // Move to the new position
    void move();

    uint8_t getServoPosX(){ return servoPosX; }
    uint8_t getServoPosY(){ return servoPosY; }
    HServo *getServoX(){ return &servoX; }
    HServo *getServoY(){ return &servoY; }

  private:
    uint8_t servoPinX;
    uint8_t servoPinY;
    uint8_t servoPosX = 0;
    uint8_t servoPosY = 0;
    HServo servoX = HServo();
    HServo servoY = HServo();
};


#endif //MOVINGHEAD_MOVEMENT_H

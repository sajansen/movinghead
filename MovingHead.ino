#include "main.h"
#include <Wire.h>

void handleCommunicationData(uint8_t command, uint8_t data);

void storeWireDataIntoBuffer(int dataLength);

uint8_t popWireDataBuffer();

// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_RGB     Pixels are wired for RGB bitstream
//   NEO_GRB     Pixels are wired for GRB bitstream
//   NEO_KHZ400  400 KHz bitstream (e.g. FLORA pixels)
//   NEO_KHZ800  800 KHz bitstream (e.g. High Density LED strip)
NeoPixel neoPixel = NeoPixel(NEOPIXEL_PIN, 10);

Movement movement = Movement(SERVO_PIN_X, SERVO_PIN_Y);

MovingHead movingHead = MovingHead(&movement, &neoPixel);

Demo demo = Demo(&movingHead);

#define INPUT_START_CHAR 91   // [
#define INPUT_SEP_CHAR 44     // ,
#define INPUT_STOP_CHAR 93    // ]

#if false
/**
 * Display a byte using the LED circle.
 * @param value         The byte to be displayed (1 = color; 0 = off)
 * @param startIndex    LED where the byte display must start
 * @param color         The color for the LED's for this byte
 */
void displayByte(uint8_t value, int startIndex, uint32_t color) {
    for (int i = 0; i < 8; i++) {
        if ((value >> i) & 1) {
            neoPixel.setPixels(startIndex + i, 1, color);
        } else {
            neoPixel.setPixels(startIndex + i, 1, 0);
        }
    }
    neoPixel.show();
}

void displayByte(uint8_t value, uint8_t startIndex) {
    displayByte(value, startIndex, 0xFFFFFF);
}
#endif

boolean runDemo = false;

void setup() {
#if USE_SERIAL
    Serial.begin(BAUD_RATE);
    Serial.setTimeout(100);
#endif

#if USE_I2C
    Wire.begin(I2C_ADDRESS);
    Wire.onReceive(storeWireDataIntoBuffer);
#endif

    movingHead.begin();

    movingHead.setColor(NeoPixel::Color(0, 0, 0));
    movingHead.setEffect(NeoPixel::SOLID);
    movingHead.setEffectSpeed(15);
    movingHead.setBrightness(5);
    movingHead.update();
    delay(100);

    // Init demo or not on the hand of an external switch (useful in case no Serial controller is available)
    pinMode(EXTERNAL_DEMO_SWITCH_PIN, INPUT);
    if (digitalRead(EXTERNAL_DEMO_SWITCH_PIN))
        runDemo = true;
}

#if USE_I2C
volatile uint8_t wireDataBuffer[I2C_DATA_BUFFER_SIZE];
volatile uint8_t wireDataBufferLength = 0;

/**
 * Reading and storing the data from I2C into a buffer
 * @param dataLength
 */
void storeWireDataIntoBuffer(int dataLength) {
    while (Wire.available()) {
        if (wireDataBufferLength >= I2C_DATA_BUFFER_SIZE) {
            // Data buffer is full
            return;
        }

        wireDataBuffer[wireDataBufferLength++] = Wire.read();
    }
}

/**
 * If I2C data is available in the buffer, read it and do something with it
 */
void handleWireCommunicationData() {
    bool movingHeadNeedsUpdate = false;

    uint8_t maxCommandLength = 2;
    while (wireDataBufferLength >= maxCommandLength) {
        // Search for the first valid meta byte
        uint8_t metaByte = popWireDataBuffer();
        if (!isValidMetaByte(metaByte)) {
            continue;
        }

        uint8_t command = metaByte & 0b00011111U;
        uint8_t data = popWireDataBuffer();
        handleCommunicationData(command, data);
        movingHeadNeedsUpdate = true;
    }

    if (movingHeadNeedsUpdate) {
        movingHead.update();
    }
}

/**
 * Remove and returns first element of the data buffer array
 * @return
 */
uint8_t popWireDataBuffer() {
    if (wireDataBufferLength == 0) {
        return 0;
    }

    noInterrupts(); // Disable interrupts to prevent the I2C interrupt from changing the same data
    uint8_t pop = wireDataBuffer[0];
    for (uint8_t i = 0; i < wireDataBufferLength - 1; i++) {
        wireDataBuffer[i] = wireDataBuffer[i + 1];
    }
    wireDataBufferLength--;
    interrupts();

    return pop;
}

#endif

#if USE_SERIAL
void SerialReadFlush() {
    while (Serial.available()) {
        Serial.read();
    }
}

/**
 * The function for receiving commands from a Serial controller, using the MovingHeadCoummunicationProtocol
 */
void handleSerialCommunicationInput() {
    if (!Serial.available()) {
        return;
    }

    uint8_t maxBufferLength = 2;
    uint8_t receiveBuffer[maxBufferLength];
    int inputLength = Serial.readBytes(receiveBuffer, maxBufferLength);

    // Input validation
    // Check data length
    if (inputLength != maxBufferLength) {
        return;
    }

    handleCommunicationData(&receiveBuffer);

    movingHead.update();
}
#endif

void handleCommunicationData(uint8_t command, uint8_t data) {
    // Save the data
    switch (command) {
        case MH_CMD_POSITION_X:
            movingHead.setPositionX(data);
            break;
        case MH_CMD_POSITION_Y:
            movingHead.setPositionY(data);
            break;
        case MH_CMD_COLOR_R:
            movingHead.setColorR(data);
            break;
        case MH_CMD_COLOR_G:
            movingHead.setColorG(data);
            break;
        case MH_CMD_COLOR_B:
            movingHead.setColorB(data);
            break;
        case MH_CMD_EFFECT:
            movingHead.setEffect(data);
            break;
        case MH_CMD_EFFECT_SPEED:
            movingHead.setEffectSpeed(data);
            break;
        case MH_CMD_BRIGHTNESS:
            movingHead.setBrightness(data);
            break;
        case MH_CMD_DEMO:
            runDemo = data & (uint8_t) 1;
            break;
        default:
            return; // No valid command to be processed
    }
}

void loop() {
#if USE_SERIAL
    handleSerialCommunicationInput();
#endif

#if USE_I2C
    handleWireCommunicationData();
#endif

    if (runDemo)
        demo.run();

    // Refresh neoPixel (do not remove or there will be no light)
    neoPixel.light();
}

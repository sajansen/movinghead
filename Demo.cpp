//
// Created by S. Jansen on 24-11-2018.
//

#include "Demo.h"

Demo::Demo(MovingHead *movingHead) {
  this->movingHead = movingHead;
  movingHead->setBrightness(10);
}

void Demo::nextLightState() {
  lightState = (++lightState % DLS_STATE_LENGTH);
  newLightState = (DemoLightState) lightState;
}

void Demo::nextMoveState() {
  moveState = (++moveState % DMS_STATE_LENGTH);
  newMoveState = (DemoMoveState) moveState;
}

void Demo::run(){
  /**
   * Light Demo
   * Only needs to update every demoTime seconds
   */
  uint16_t now = millis();
  if (now - lastTimeLight > demoTimeLight){
    lastTimeLight = now;
    switch (newLightState){
      case DLS_SMILEY:
        // Start with a smile :)
        movingHead->setColor(NeoPixel::Color(255, 50, 255));
        movingHead->setEffect(NeoPixel::SMILEY);
        movingHead->setEffectSpeed(15);
        break;
      case DLS_STROBE:
        movingHead->setColor(NeoPixel::Color(255, 255, 255));
        movingHead->setEffect(NeoPixel::STROBE);
        movingHead->setEffectSpeed(100);
        break;
      case DLS_STARS:
        movingHead->setColor(NeoPixel::Color(255, 255, 255));
        movingHead->setEffect(NeoPixel::STARS);
        movingHead->setEffectSpeed(30);
        break;
      case DLS_WHITE_LIGHT:
        movingHead->setColor(NeoPixel::Color(255, 255, 255));
        movingHead->setEffect(NeoPixel::SOLID);
        break;
      case DLS_RAINBOW:
        movingHead->setEffect(NeoPixel::RAINBOW);
        movingHead->setEffectSpeed(0);
        break;
      default:
        break;
    }
    nextLightState();
  }

  /**
   * Movement Demo
   */
  now = millis();
  if (now - lastTimeMovement > intervalTimeMovement) {
    lastTimeMovement = now;
    uint16_t maxDegreesX = movingHead->getMovement()->getServoX()->getMaxDegrees();
    uint16_t maxDegreesY = movingHead->getMovement()->getServoY()->getMaxDegrees();
    int16_t newPositionX = movingHead->getPositionX() + counterX;
    int16_t newPositionY = movingHead->getPositionY() + counterY;

    // Set boundaries
    if (newPositionX >= maxDegreesX || newPositionX <= 0)
      counterX *= -1;
    if (newPositionX > maxDegreesX)
      newPositionX = maxDegreesX;
    if (newPositionX < 0)
      newPositionX = 0;

    if (newPositionY >= maxDegreesY || newPositionY <= 0)
      counterY *= -1;
    if (newPositionY > maxDegreesY)
      newPositionY = maxDegreesY;
    if (newPositionY < 0)
      newPositionY = 0;

    switch (newMoveState) {
      case DMS_ALL_SHOCKY:
        counterX = (int16_t) random(35, 60) * (counterX > 0 ? 1 : -1);
        counterY = (int16_t) random(35, 60) * (counterY > 0 ? 1 : -1);
      case DMS_ALL_SMOOTH:
        movingHead->setPositionX((uint8_t) newPositionX);
        movingHead->setPositionY((uint8_t) newPositionY);
        break;
      case DMS_X_SMOOTH:
        movingHead->setPositionX((uint8_t) newPositionX);
        movingHead->setPositionY((uint8_t) 147);
        break;
      case DMS_Y_SMOOTH:
        movingHead->setPositionX((uint8_t) 90);
        movingHead->setPositionY((uint8_t) newPositionY);
        break;
      case DMS_XY_SMOOTH:
        newPositionX = newPositionY;
        // Make it go diagonal by reversing the X position half way
        if (newPositionX > maxDegreesX / 2){
          newPositionX = maxDegreesX - newPositionY;
        }
        movingHead->setPositionX((uint8_t) newPositionX);
        movingHead->setPositionY((uint8_t) newPositionY);
        break;
      default:
        break;
    }
    movingHead->update();
  }

  if (lastDemoTimeMovement == NULL || now - lastDemoTimeMovement > demoTimeMovement) {
    // Let the frist state ever be the first in the enum list, so skip getting a new state
    if (lastDemoTimeMovement != NULL)
      nextMoveState();

    lastDemoTimeMovement = now;   // Tell when this demo state has started

    /**
     * Init new state
     */
    switch (newMoveState) {
      case DMS_XY_SMOOTH:
        movingHead->setPositionX(0);
        movingHead->setPositionY(0);
      case DMS_ALL_SMOOTH:
      case DMS_X_SMOOTH:
      case DMS_Y_SMOOTH:
        // Do update every x milliseconds
        intervalTimeMovement = 10;

        // Set increaser
        counterX = 2;
        counterY = 2;
        break;
      case DMS_ALL_SHOCKY:
        // Do update every x milliseconds
        intervalTimeMovement = 800;

        // Set increaser
        counterX = (int16_t) random(35, 60);
        counterY = (int16_t) random(35, 60);
        break;
      default:
        counterX = 0;
        counterY = 0;
        break;
    }

    // Give random movement direction
    if (random(0, 1))
      counterX *= -1;
    if (random(0, 1))
      counterY *= -1;
  }
}
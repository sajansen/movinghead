//
// Created by S. Jansen on 30-11-2018.
//

#include "HServo.h"

void HServo::attach(uint8_t pin) {
    if (pin != 9 && pin != 10) {
        // Invalid PWM pins
        return;
    }
    this->pin = pin;

    pinMode(this->pin, OUTPUT);
    minDutyCycle = (uint16_t) (MAX_DUTYCYCLE * minDutyCycleSeconds / PERIOD);
    maxDutyCycle = (uint16_t) (MAX_DUTYCYCLE * maxDutyCycleSeconds / PERIOD);

    timer->pwm(this->pin, dutyCycle);

    timer->start();
}

void HServo::write(uint8_t value) {
    // Add, to make the client able to just write 0 -> 180...
    value += minDegreesLimit;
    if (value > (maxDegreesLimit - minDegreesLimit))
        value = maxDegreesLimit - minDegreesLimit;

    dutyCycle = (uint16_t) map(value, minDegrees, maxDegrees, minDutyCycle, maxDutyCycle);
    timer->setPwmDuty(pin, dutyCycle);
}

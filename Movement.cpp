//
// Created by S. Jansen on 24-11-2018.
//

#include "Movement.h"

Movement::Movement(uint8_t servoPinX, uint8_t servoPinY){
  this->servoPinX = servoPinX;
  this->servoPinY = servoPinY;
}

void Movement::begin(){
  Timer1.initialize(PERIOD);

  this->servoX.attach(this->servoPinX);
  this->servoY.attach(this->servoPinY);
  move();
}

void Movement::toX(uint8_t pos){
  servoPosX = pos;
  move();
}

void Movement::toY(uint8_t pos){
  servoPosY = pos;
  move();
}

void Movement::move(){
  servoX.write(servoPosX);
  servoY.write(servoPosY);
}
//
// Created by S. Jansen on 24-11-2018.
//

#ifndef MOVINGHEAD_DEMO_H
#define MOVINGHEAD_DEMO_H


#include <Arduino.h>
#include "MovingHead.h"
#include "NeoPixel.h"

class Demo {
    enum DemoLightState{
      DLS_SMILEY,
      DLS_STROBE,
      DLS_STARS,
      DLS_WHITE_LIGHT,
      DLS_RAINBOW,
      DLS_STATE_LENGTH  // Do not remove
    };
    enum DemoMoveState{
      DMS_ALL_SMOOTH,
      DMS_ALL_SHOCKY,
      DMS_X_SMOOTH,
      DMS_Y_SMOOTH,
      DMS_XY_SMOOTH,
      DMS_STATE_LENGTH  // Do not remove
    };
  public:
    Demo(MovingHead *movingHead);
    void run();

  private:
    void nextLightState();
    void nextMoveState();

    MovingHead *movingHead;
    uint16_t demoTimeLight = 5000;
    uint16_t lastTimeLight = 0;
    DemoLightState newLightState;
    uint16_t lightState = 0;

    uint16_t demoTimeMovement = 7000;
    uint16_t lastDemoTimeMovement = NULL;
    uint16_t intervalTimeMovement = 50;
    uint16_t lastTimeMovement = 0;
    DemoMoveState newMoveState = DMS_ALL_SMOOTH;
    uint16_t moveState = 0;
    int16_t counterX = 1;
    int16_t counterY = 1;
};


#endif //MOVINGHEAD_DEMO_H

//
// Created by S. Jansen on 24-11-2018.
//

#ifndef NEOPIXEL_H
#define NEOPIXEL_H

#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

class NeoPixel: public Adafruit_NeoPixel {
  public:
    enum Effect {
      OFF,
      SOLID,
      STROBE,
      STARS,
      SMILEY,
      RAINBOW
    };

    // Default constructor
    NeoPixel(uint16_t n, uint8_t p, uint8_t t);
    // More specific constructor
    NeoPixel(uint8_t p, uint8_t b);
    void begin();

    // Tools
    uint16_t toStripBound(uint16_t pixel);
    uint16_t toSmoothStripBound(uint16_t position);
    uint16_t getSmoothness();

    void clearAll();
    void blackAll();
    void setPixels(uint8_t startPixel, uint8_t pixelLength, uint32_t pixelColor);
    void showPixels(uint8_t startPixel, uint8_t pixelLength, uint32_t pixelColor);
    void showPixelsFaded(uint8_t startPixel, uint8_t pixelLength, uint32_t pixelColor);
    void setSmoothPixels(uint8_t startPosition, uint8_t pixelLength, uint32_t pixelColor);
    void setSmoothness(uint16_t smoothness);

    // Methods
    void setColor(uint32_t color);
    void setEffect(Effect effect);
    void setEffectSpeed(uint16_t effectSpeed);
    void setBrightness(uint8_t brightness);
    void light();

    // Preprogrammed
    void pingpong();
    void smiley(uint16_t offset);
    void spinSmiley();
    void smoothSmiley(uint16_t offset);
    void spinSmoothSmiley(uint16_t delayTime);

  private:
    boolean needUpdate = true;
    unsigned long lastUpdateTime = 0;

    uint16_t smoothness = 10;
    uint16_t currentPosition = 0;
    uint32_t color;
    NeoPixel::Effect effect;
    uint16_t effectSpeed = 500;  // In milliseconds

    uint32_t rainbowColorR = 0;
    uint32_t rainbowColorG = 0;
    uint32_t rainbowColorB = 0;
};

#endif //NEOPIXEL_H

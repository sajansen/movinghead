# MovingHead

_Build your own MovingHead using minimal and cheap components (like Arduino)_

### Setup

#### Hardware

##### Requirements

* Arduino (Nano is fine).
* 2x servo's (one for X and one for Y axis).
* NeoPixel LED ring.
* Skills to create the casing including the axis.
* \>=3 pole plug (for communication with external Serial Controller, if any).
* (Switch to toggle on/off demo mode at startup.)

Use your good sens to put everything together: 

* Attach each servo's data pin to the Arduino on a PWM output pin.
* Attach the NeoPixel LED ring's data pin to the Arduino.
* Connect two of the three terminals of the external Serial Controller plug to the Arduino's RX and TX pins. Connect the third terminal to the same ground the Arduino is using.
* (Place the demo switch between +5V and some Arduino ditigal input pin. Connect this pin also to ground with a pull down resistor (~10k ohm).
 
Edit [main.h](main.h) with the right pins. 

##### Power

Make sure that at least the LED ring is on an external power supply. Preferably connect everything to an external power supply, so it does not use the +5V from your computer's USB port. That port won't be able to deliver enough current. For programming the Arduino you can leave it connected, just make sure there's a common ground.
 
 
#### Software

**Configuration**

As said before, make sure you've set the pins in [main.h](main.h) right. You may also need to check the settings of the NeoPixel class in [MovingHead.ino](MovingHead.ino). 

**Libraries**

It may be that you also need to import the [Adafruit_NeoPixel](lib/Adafruit_NeoPixel) and [MovingHeadCommunicationProtocol](https://bitbucket.org/sajansen/movingheadcommunicationprotocol/) as a Library. The TimerOne library is included in the project files, so you won't need to add that one as a library.

The MovingHeadCommunicationProtocol is a git submodule, so you need to check that one out by running:
```bash
git submodule update --init --recursive
```

Then just load the [MovingHead.ino](MovingHead.ino) sketch with your Arduino IDE. 

Upload and go play! 

### Control

The MovingHead can be controlled in three ways:

##### External Serial Controller

This is the preferable way, because it is the fastest and the design is focused on this. 

Create or use an Serial Controller which communicates with the MovingHead using the serial ports and the MovingHeadCommunicationProtocol. An example is this project, using a touchscreen to take control of the MovingHead: [LCDMovingHeadController](https://bitbucket.org/sajansen/lcdmovingheadcontroller/).

##### Serial Monitor on your Computer

You can do this, but it will be slow and very clumsy. Connect your MovingHead to a computer with a serial monitor. 
You can send the MovingHead string commands in the format:

`[3:POSITION_X,3:POSITION_Y,3:COLOR_R,3:COLOR_G,3:COLOR_B,3:EFFECT,5:EFFECT_SPEED,3:BRIGHTNESS]`

Where each argument between the brackets stands for: `length:value`. So to update the MovingHead X position to 90 degrees, send: `[090]`.
To update the MovingHead Y position to 110 for example, the preceding arguments must also be send with: `[090,110]`.

##### External Demo Switch

If you've connected a switch to the Demo input pin of the Arduino, you can tell the MovingHead to go into Demo mode at power-on. During runtime, you can switch this mode off but not on. 


### Future

In the future, I want to be able to use DMX input for controlling the MovingHead. The software is designed in such a way, that implementing the DMX protocol must be very easy. But, due to the lack of a DMX shield, implementing the protocol was not possible for me.


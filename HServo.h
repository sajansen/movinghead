//
// Created by S. Jansen on 30-11-2018.
//

#ifndef MOVINGHEAD_SERVO_H
#define MOVINGHEAD_SERVO_H

#include <Arduino.h>
#include "TimerOne.h"

#define PERIOD 20000  // Microseconds
//#define PERIOD 500000  // Microseconds
#define MAX_DUTYCYCLE 1023

class HServo {
  public:
    void attach(uint8_t pin);

    /**
     * Turn the servo to a certain degree (minDegrees - maxDegrees: see below)
     * @param value
     */
    void write(uint8_t value);
    uint16_t getMinDegrees(){ return minDegreesLimit; }
    uint16_t getMaxDegrees(){ return maxDegreesLimit; }
  private:
    uint8_t pin;
    TimerOne *timer = &Timer1;
    uint16_t dutyCycle = 0;
    uint32_t minDutyCycleSeconds = 480;  // Microseconds
    uint32_t maxDutyCycleSeconds = 2500;  // Microseconds
    uint16_t minDutyCycle;
    uint16_t maxDutyCycle;

    // The minimun and maximum degrees which can be chosen using ->write()
    uint16_t minDegrees = 0;
    uint16_t maxDegrees = 205;

    // Internal corrections to the received degrees. minDegrees is the actual value for the servo to turn to 0 degrees
    uint16_t minDegreesLimit = 5;
    uint16_t maxDegreesLimit = 180;
};


#endif //MOVINGHEAD_SERVO_H

//
// Created by S. Jansen on 24-11-2018.
//

#include "NeoPixel.h"


NeoPixel::NeoPixel(uint16_t n, uint8_t p, uint8_t t) : Adafruit_NeoPixel(n, p = 6, t = NEO_GRB + NEO_KHZ800) {}

NeoPixel::NeoPixel(uint8_t p, uint8_t b) : Adafruit_NeoPixel(24, p, NEO_GRB + NEO_KHZ800) {
    setBrightness(b);
}

void NeoPixel::begin() {
    Adafruit_NeoPixel::begin();
    show(); // Initialize all pixels to 'off'
    setBrightness(10);
}

/**
 * TOOLS
 */

uint16_t NeoPixel::toStripBound(uint16_t pixel) {
    // Make positive / set lower bounds
    while (pixel < 0) {
        pixel += numPixels();
    }
    // Set upper bounds
    pixel = pixel % numPixels();
    return pixel;
}

uint16_t NeoPixel::toSmoothStripBound(uint16_t position) {
    // Make positive / set lower bounds
    while (position < 0) {
        position += smoothness * numPixels();
    }
    // Set upper bounds
    position = position % (smoothness * numPixels());
    return position;
}

uint16_t NeoPixel::getSmoothness() {
    return smoothness;
}

void NeoPixel::clearAll() {
    for (uint16_t i = 0; i < numPixels(); i++) {
        setPixelColor(i, 0);
    }
}

void NeoPixel::blackAll() {
    clearAll();
    show();
}

void NeoPixel::setPixels(uint8_t startPixel, uint8_t pixelLength, uint32_t pixelColor) {
    for (uint8_t i = startPixel; i < startPixel + pixelLength; i++) {
        setPixelColor(toStripBound(i), pixelColor);
    }
}

void NeoPixel::showPixels(uint8_t startPixel, uint8_t pixelLength, uint32_t pixelColor) {
    setPixels(startPixel, pixelLength, pixelColor);
    show();
}

void NeoPixel::showPixelsFaded(uint8_t startPixel, uint8_t pixelLength, uint32_t pixelColor) {
    // Set pixels
    for (uint8_t i = startPixel; i < startPixel + pixelLength; i++) {
        setPixelColor(i, pixelColor);
    }

    // Set faded pixels
    uint8_t r = (uint8_t) (pixelColor >> 16);
    uint8_t g = (uint8_t) (pixelColor >> 8);
    uint8_t b = (uint8_t) pixelColor;
    r = r / 3;
    g = g / 3;
    b = b / 3;

    uint16_t prevPixel = startPixel - 1;
    uint16_t nextPixel = startPixel + pixelLength;
    if (prevPixel < 0) {
        prevPixel = numPixels() - 1;
    } else if (prevPixel > numPixels()) {
        prevPixel = 0;
    }
    if (nextPixel < 0) {
        nextPixel = numPixels() - 1;
    } else if (nextPixel > numPixels()) {
        nextPixel = 0;
    }
    setPixelColor(prevPixel, Color(r, g, b));
    setPixelColor(nextPixel, Color(r, g, b));

    show();
}

void NeoPixel::setSmoothPixels(uint8_t startPosition, uint8_t pixelLength, uint32_t pixelColor) {
    uint8_t factor = startPosition % smoothness;
    uint8_t startPixel = startPosition / smoothness;

    uint8_t R = (uint8_t) (pixelColor >> 16);
    uint8_t G = (uint8_t) (pixelColor >> 8);
    uint8_t B = (uint8_t) pixelColor;

    uint8_t stopR = R * factor / smoothness;
    uint8_t stopG = G * factor / smoothness;
    uint8_t stopB = B * factor / smoothness;

    uint8_t startR = R - stopR;
    uint8_t startG = G - stopG;
    uint8_t startB = B - stopB;

    // Set start pixel faded color
    setPixelColor(toStripBound(startPixel), Color(startR, startG, startB));
    // Everything in between
    for (uint8_t i = startPixel + 1; i < startPixel + pixelLength; i++) {
        setPixelColor(toStripBound(i), pixelColor);
    }
    // Set stop pixel faded color
    setPixelColor(toStripBound(startPixel + pixelLength), Color(stopR, stopG, stopB));

}

void NeoPixel::setSmoothness(uint16_t smoothness) {
    this->smoothness = smoothness;
}


/**
 * METHODS
 */

void NeoPixel::setColor(uint32_t color) {
    this->color = color;
    needUpdate = true;
}

void NeoPixel::setEffect(NeoPixel::Effect effect) {
    this->effect = effect;
    needUpdate = true;
}

void NeoPixel::setEffectSpeed(uint16_t speed) {
    this->effectSpeed = speed;
}

void NeoPixel::setBrightness(uint8_t brightness) {
    Adafruit_NeoPixel::setBrightness(brightness);
}

void NeoPixel::light() {
    unsigned long now = millis();
    switch (effect) {
        case SOLID:
            if (!needUpdate) break;

            setPixels(0, numPixels(), this->color);
            show();
            break;
        case STROBE:
            // Check if update is needed to prevent overloading the system  todo: replace with timer!
            if (now - lastUpdateTime <= effectSpeed) break;
            lastUpdateTime = now;

            // Show light only half of the time
            if ((now / effectSpeed) % 2) {
                setPixels(0, numPixels(), this->color);
            } else {
                clearAll();
            }
            show();
            break;
        case STARS:
            // Check if update is needed to prevent overloading the system  todo: replace with timer!
            if (now - lastUpdateTime <= effectSpeed) break;
            lastUpdateTime = now;

            // Refresh half of the time
            if ((now / effectSpeed) % 2) {
                clearAll();
                uint8_t maxStars = random(1, 3);  // Toggle between max 1 and 3 stars per frame
                for (uint8_t i = 0; i < maxStars; i++) {
                    // Set a random pixel
                    setPixels(random(0, numPixels()), 1, this->color);
                }
                show();
            }
            break;
        case SMILEY:
            if (now - lastUpdateTime <= effectSpeed) break;
            lastUpdateTime = now;

            spinSmoothSmiley(effectSpeed);
            show();
            break;
        case RAINBOW:
            if (now - lastUpdateTime <= effectSpeed) break;
            lastUpdateTime = now;

            if (rainbowColorG > 0 && rainbowColorR == 255) {
                rainbowColorG -= 5;
            } else if (rainbowColorB < 255 && rainbowColorR == 255) {
                rainbowColorB += 5;
            } else if (rainbowColorB == 255 && rainbowColorR > 0) {
                rainbowColorR -= 5;
            } else if (rainbowColorB == 255 && rainbowColorG < 255) {
                rainbowColorG += 5;
            } else if (rainbowColorB > 0 && rainbowColorG == 255) {
                rainbowColorB -= 5;
            } else if (rainbowColorG == 255 && rainbowColorR < 255) {
                rainbowColorR += 5;
            } else {
                rainbowColorB += 5;
            }

            setPixels(0, numPixels(), NeoPixel::Color(rainbowColorR, rainbowColorG, rainbowColorB));
            show();
            break;
        case OFF:
        default:
            blackAll();
    }
}


/**
 * PRE PROGRAMMED SHOWS
 */

void NeoPixel::pingpong() {
    uint8_t pingDelay = 20;
    uint8_t pongDelay = pingDelay;

    for (uint16_t i = 0; i < numPixels(); i++) {
        clearAll();
        showPixelsFaded(i, 1, Color(0, 255, 0));
        delay(pingDelay);
    }
    for (int16_t i = numPixels() - 1; i >= 0; i--) {
        clearAll();
        showPixelsFaded(i, 1, Color(0, 255, 0));
        delay(pongDelay);
    }
}

void NeoPixel::smiley(uint16_t offset) {
    uint32_t eyeColor = Color(50, 50, 255);
    uint32_t skinColor = Color(255, 213, 0);

    uint16_t eyeRight = offset + 1;
    uint16_t eyeLeft = offset + 20;
    uint16_t mouth = offset + 6;

    setPixels(eyeRight, 2, eyeColor);
    setPixels(eyeLeft, 2, eyeColor);
    setPixels(mouth, 11, skinColor);
    show();
}

void NeoPixel::spinSmiley() {
    for (uint16_t i = 0; i < numPixels(); i++) {
        clearAll();
        smiley(i);
        delay(200);
    }
}

void NeoPixel::smoothSmiley(uint16_t offset) {
    uint32_t eyeColor = Color(50, 50, 255);
    uint32_t skinColor = Color(255, 213, 0);

    uint16_t eyeRight = offset + 1 * smoothness;
    uint16_t eyeLeft = offset + 20 * smoothness;
    uint16_t mouth = offset + 6 * smoothness;

    setSmoothPixels(toSmoothStripBound(eyeRight), 1, eyeColor);
    setSmoothPixels(toSmoothStripBound(eyeLeft), 1, eyeColor);
    setSmoothPixels(toSmoothStripBound(mouth), 10, skinColor);
}

void NeoPixel::spinSmoothSmiley(uint16_t delayTime = 15) {
    currentPosition++;
    if (currentPosition >= smoothness * numPixels()) {
        currentPosition = 0;
    }
    clearAll();
    smoothSmiley(currentPosition);
}